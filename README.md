This is head track project build using A-Star 32U4 Micro 5V/16MHz (like Arduino Micro) with L3GD20H 3-Axis Gyro Carrier, accelerometer and magnetometer.
The Gyroscope with accelerometer and magnetometer:
https://www.pololu.com/product/2738
Main board:
https://www.pololu.com/product/3101

The hardware part:
Simply connect the SCL and SDA (I²C) from main board (Arduino Micro) to the SCL and SDA on the L3GD20H board.
We also need power supply, co connect 5V (out) and GND from main board to VIN and GND of the L3GD20H board.
To supply main board connect micro-usb to it and start arduino IDE (http://www.arduino.org/downloads).

The head tracking system is build from three patrs:
- hardware - gyroscope connected to arduino that sends gyro data using COM port.
- firmware - gyroscope program uploaded from the arduino IDE (file in repository : "gyroscope_firmware_arduino")
- GyroTrack software - used to gather data from COM port gyroscope hardware and send data using UDP protocol to opentrack
- opentrack 2.3.1.5 - software similar to freetrackir - I use freetrack2.0 output from it in games https://github.com/opentrack/opentrack/releases