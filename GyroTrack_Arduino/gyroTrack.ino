#include <Wire.h>
#include <LSM6.h>
#include <LIS3MDL.h>

#include "Kalman.h" // Source: https://github.com/TKJElectronics/KalmanFilter

#define GYRO_TO_DEG_S 100.0 // Convert to deg/s 131.0

//When BUILD_FOR_CALIBRATION note all three (x,y,z) magnetometer offset values below:

//Number of samples for average filter (only for gyro offset calculation):
#define FILTER_SIZE 100

LSM6 imu;
LIS3MDL mag;
double magneto_offset;
int incomingByte = 0;   // for incoming serial data

Kalman kalmanX;
Kalman kalmanY;
Kalman kalmanZ;

double accX, accY, accZ;
double gyroX, gyroY, gyroZ;
int16_t tempRaw;

double gyroXangle,gyroYangle,gyroZangle;
double gyroXoffset, gyroYoffset, gyroZoffset;
double kalAngleX, kalAngleY, kalAngleZ;

double magXoffset = 0;
double magYoffset = 0;
double magZoffset = 0;

double roll;
double yaw;
double pitch;

uint32_t timer;

enum show{once = 0,looped = 1};

void showOffsets(bool loop_display){
  while(1){
    Serial.print(gyroXoffset);
    Serial.print(":");
    Serial.print(gyroYoffset);
    Serial.print(":");
    Serial.print(gyroZoffset);
    Serial.print(":");
    Serial.print(magXoffset);
    Serial.print(":");
    Serial.print(magYoffset);
    Serial.print(":");
    Serial.println(magZoffset);
    delay(10);
    if(!loop_display) break;
    if (Serial.available() > 0) {
      if(Serial.read()=='F')break;
      }
    }
}

//IMPORTANT!!! During calibration of gyroscope stand still and do not move the device!
void calibrateGyroOffset(bool loop_display){
  double gXfilter;
  double gYfilter;
  double gZfilter;
  for(int i=0; i<FILTER_SIZE; i++){
    imu.read();
    gXfilter += imu.g.x;
    gYfilter += imu.g.y;
    gZfilter += imu.g.z;
    delay(10);
  }
  gyroXoffset=gXfilter/FILTER_SIZE;
  gyroYoffset=gYfilter/FILTER_SIZE;
  gyroZoffset=gZfilter/FILTER_SIZE;

  initialize();
  showOffsets(loop_display);
}

void calibrateMagnetOffset(){
  double magXmax=-100000;
  double magXmin=100000;
  double magYmax=-100000;
  double magYmin=100000;
  double magZmax=-100000;
  double magZmin=100000;
  while(1){
     mag.read();
    if(mag.m.x>magXmax)magXmax=mag.m.x;
    if(mag.m.x<magXmin)magXmin=mag.m.x;
    if(mag.m.y>magYmax)magYmax=mag.m.y;
    if(mag.m.y<magYmin)magYmin=mag.m.y;
    if(mag.m.z>magZmax)magZmax=mag.m.z;
    if(mag.m.z<magZmin)magZmin=mag.m.z;
    
   //FOR MAGNETOMETER CALIBRATION xoffset=(minX+maxX)/2 , yoffset=(minY+maxY)/2, zoffset=(minZ+maxZ)/2:
    magXoffset = (magXmin+magXmax)/2;
    magYoffset = (magYmin+magYmax)/2;
    magZoffset = (magZmin+magZmax)/2;
    
    //IMPORTANT!!! During calibration of magnetometer turn the sensor in all angles till none of the offset values is changing.
    //Do not callibrate near metal objects or magnets!
    showOffsets(false);
    if (Serial.available() > 0) {
    if(Serial.read()=='F'){
      initialize();
      break;
      }
    }
  }
}

void setup()
{
  Serial.begin(115200);
  Wire.begin();

  if (!mag.init())
  {
    Serial.println("Failed to detect and initialize magnetometer!");
    while (1);
  }
  mag.enableDefault();

  if (!imu.init())
  {
    Serial.println("Failed to detect and initialize IMU!");
    while (1);
  }
  imu.enableDefault();
  delay(100);

  bool loop_display = false;
  calibrateGyroOffset(show::once);
  initialize();
}
void initialize(){
  kalmanX.restart();
  kalmanY.restart();
  kalmanZ.restart();
  
  imu.read();
  accX = imu.a.x;
  accY = imu.a.y;
  accZ = imu.a.z;
  
  double roll  = atan2(accY, accZ) * RAD_TO_DEG;
  double pitch = atan(-accX / sqrt(accY * accY + accZ * accZ)) * RAD_TO_DEG;
  
  double cosPitch = cos(pitch/RAD_TO_DEG);
  double sinPitch = sin(pitch/RAD_TO_DEG);
  double cosRoll = cos(roll/RAD_TO_DEG);
  double sinRoll = sin(roll/RAD_TO_DEG);
  mag.read();
  double magX = (mag.m.x - magXoffset) * cosPitch + (mag.m.z-magZoffset) * sinPitch;
  double magY = (mag.m.x - magXoffset) * sinRoll * sinPitch + (mag.m.y - magYoffset) * cosRoll - (mag.m.z-magZoffset) * sinRoll * cosPitch;
  double heading = atan2(magY,magX)*RAD_TO_DEG;
  yaw = heading;

  //Set starting angles:
  kalmanX.setAngle(roll); 
  kalmanY.setAngle(pitch);
  kalmanZ.setAngle(yaw);
  gyroXangle = roll;
  gyroYangle = pitch;
  gyroZangle = yaw; 

  //Start time measurement:
  timer = micros();
}

void loop()
{
  imu.read();
  accX = imu.a.x;
  accY = imu.a.y;
  accZ = imu.a.z;
  gyroX = imu.g.x-gyroXoffset;
  gyroY = imu.g.y-gyroYoffset;
  gyroZ = imu.g.z-gyroZoffset;

  double dt = (double)(micros() - timer) / 1000000; //delta time
  timer = micros();

  double roll  = atan2(accY, accZ) * RAD_TO_DEG;
  double pitch = atan(-accX / sqrt(accY * accY + accZ * accZ)) * RAD_TO_DEG;

  double gyroXrate = gyroX / GYRO_TO_DEG_S; // Convert to deg/s
  double gyroYrate = gyroY / GYRO_TO_DEG_S; // Convert to deg/s
  double gyroZrate = -gyroZ / GYRO_TO_DEG_S; // Convert to deg/s

  //Fix problem when the accelerometer angle is between -180 / 180 degrees:
  if ((roll < -90 && kalAngleX > 90) || (roll > 90 && kalAngleX < -90)) {
    kalmanX.setAngle(roll);
    kalAngleX = roll;
    gyroXangle = roll;
  } else{
  //Calculate the angle using a Kalman filter:
    kalAngleX = kalmanX.getAngle(roll, gyroXrate, dt); 
  }

  if (abs(kalAngleX) > 90){
  //Invert rate, so it fits the restriced accelerometer reading:
    gyroYrate = -gyroYrate; 
  }
    kalAngleY = kalmanY.getAngle(pitch, gyroYrate, dt);

  //Calculate gyro angle:
  gyroXangle += gyroXrate * dt; 
  gyroYangle += gyroYrate * dt;

  // Reset the gyro when angle has drifted too much...
  if (gyroXangle < -180 || gyroXangle > 180){
  gyroXangle = kalAngleX;}
  if (gyroYangle < -180 || gyroYangle > 180){
  gyroYangle = kalAngleY;}

      double cosPitch = cos(pitch/RAD_TO_DEG);
      double sinPitch = sin(pitch/RAD_TO_DEG);
      double cosRoll = cos(roll/RAD_TO_DEG);
      double sinRoll = sin(roll/RAD_TO_DEG);
      mag.read();
      double magX = (mag.m.x - magXoffset) * cosPitch + (mag.m.z-magZoffset) * sinPitch;
      double magY = (mag.m.x - magXoffset) * sinRoll * sinPitch + (mag.m.y - magYoffset) * cosRoll - (mag.m.z-magZoffset) * sinRoll * cosPitch;
      double heading = atan2(magY,magX)*RAD_TO_DEG;
    
    yaw = heading;
    kalAngleZ = kalmanZ.getAngle(yaw, gyroZrate, dt);

      Serial.print(kalAngleX);//pitch
      Serial.print(":");
      Serial.print(kalAngleY);//roll
      Serial.print(":");
      Serial.println(kalAngleZ);//yaw

  if (Serial.available() > 0) {
    incomingByte = Serial.read();
    if(incomingByte=='G'){
      calibrateGyroOffset(show::looped);
    }
    if(incomingByte=='M'){
      calibrateMagnetOffset();
    }
  }
  delay(5);
}