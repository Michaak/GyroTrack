﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace GyroControls
{
    public partial class GyroControlMonitor : UserControl
    {
        public delegate void AngleChangedDelegate();
        public event AngleChangedDelegate AngleChanged;

        private Rectangle drawPanel;
        private Point centerPoint;
        private int angle;

        public GyroControlMonitor()
        {
            InitializeComponent();
            angle = 0;
            this.DoubleBuffered = true;
        }

        private void GyroControlMonitor_Load(object sender, EventArgs e)
        {
            setDrawPanel();
        }

        private void GyroControlMonitor_Resize(object sender, EventArgs e)
        {
            this.Height = this.Width;
            setDrawPanel();
        }

        private void setDrawPanel()
        {
            drawPanel = new Rectangle(0, 0, this.Width, this.Height);
            drawPanel.Width -= 5;
            drawPanel.Height -= 5;
            centerPoint = new Point(drawPanel.Width / 2, drawPanel.Height / 2);
            this.Refresh();
        }

        public int getAngle() { return angle; }

        public void setAngle(int newAngle)
        {
            angle = newAngle;
            if (!this.DesignMode && AngleChanged != null)
                AngleChanged();
            this.Refresh();
        }

        private PointF DegreesToXY(float degree, float radius, Point centerPoint)
        {
            PointF anglePoint = new PointF();
            double radians = (degree + 90) * Math.PI / 180.0;

            anglePoint.X = (float)Math.Cos(radians) * radius + centerPoint.X;
            anglePoint.Y = (float)Math.Sin(-radians) * radius + centerPoint.Y;

            return anglePoint;
        }
        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
            Graphics drawGraphics = pevent.Graphics;
            drawGraphics.SmoothingMode = SmoothingMode.AntiAlias;
            base.OnPaintBackground(pevent);

            SolidBrush fill = new SolidBrush(Color.FromArgb(20, 255, 0, 0));
            Pen outerCircle = new Pen(Color.FromArgb(44, 44, 44), 1.0f);
            Rectangle originSquare = new Rectangle(centerPoint.X - 1, centerPoint.Y - 1, 2, 2);
            drawGraphics.DrawEllipse(outerCircle, drawPanel);
            drawGraphics.DrawEllipse(outerCircle, originSquare);
            drawGraphics.FillEllipse(fill, drawPanel);

            for (int i = 0; i < 360; i += 30)
            {
                PointF m1Point = DegreesToXY(i, centerPoint.X - 4, centerPoint);
                PointF m2Point = DegreesToXY(i, centerPoint.X, centerPoint);
                drawGraphics.DrawLine(Pens.Black, m1Point, m2Point);
            }
            drawGraphics.SmoothingMode = SmoothingMode.HighSpeed;

            outerCircle.Dispose();
            fill.Dispose();
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics drawGraphics = e.Graphics;
            PointF anglePoint = DegreesToXY(angle, centerPoint.X, centerPoint);

            drawGraphics.SmoothingMode = SmoothingMode.AntiAlias;
            drawGraphics.DrawLine(Pens.Red, centerPoint, anglePoint);
            drawGraphics.SmoothingMode = SmoothingMode.HighSpeed;

            base.OnPaint(e);
        }
    }
}
