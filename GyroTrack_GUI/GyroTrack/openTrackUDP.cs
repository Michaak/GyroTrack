﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Globalization;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace GyroTrack
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct GTdata
    {
        public double x;
        public double y;
        public double z;
        public double yaw;
        public double pitch;
        public double roll;

        public byte[] ToBytes()
        {
            Byte[] bytes = new Byte[Marshal.SizeOf(typeof(GTdata))];
            GCHandle pinStructure = GCHandle.Alloc(this, GCHandleType.Pinned);
            try
            {
                Marshal.Copy(pinStructure.AddrOfPinnedObject(), bytes, 0, bytes.Length);
                return bytes;
            }
            finally
            {
                pinStructure.Free();
            }
        }

        public bool update(string data)
        {
            if (data.Length > 4)
            {
                try
                {
                    string[] values_tokens = data.Split(':');
                    if (values_tokens.Length == 3)
                    {
                        pitch = double.Parse(values_tokens[0], CultureInfo.InvariantCulture);
                        roll = double.Parse(values_tokens[1], CultureInfo.InvariantCulture);
                        yaw = double.Parse(values_tokens[2], CultureInfo.InvariantCulture);
                        return true;
                    }
                }
                catch {
                    return false;
                }
            }
            return false;
        }
    };

    public class openTrackUDP
    {
        private int port;
        private string ip_address;
        UdpClient udpClient;

        public openTrackUDP(string address, int dest_port)
        {
            port = dest_port;
            ip_address = address;
            udpClient = new UdpClient(address, dest_port);
        }
        ~openTrackUDP()
        {
            udpClient.Close();
        }

        public void sendTrack(GTdata data)
        {
            try
            {
                int size = System.Runtime.InteropServices.Marshal.SizeOf(typeof(GTdata));
                udpClient.Send(data.ToBytes(), size);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
