﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Windows.Forms;
using System.Threading;

namespace GyroTrack
{
    public partial class GyroTrackForm : Form
    {
        public delegate void ShowRawReadings(string raw_data);
        public ShowRawReadings ShowRawReadingsDelegate;
        public delegate void ShowCalibrationReadings(string cal_data);
        public ShowCalibrationReadings ShowCalibrationDelegate;
        GyroTrackProc gyroTrack;

        public GyroTrackForm()
        {
            InitializeComponent();
            string[] com_ports = SerialPort.GetPortNames();
            comboBoxCOMports.Items.AddRange(com_ports);

            ShowRawReadingsDelegate = new ShowRawReadings(UpdateGyroTrackReadings);
            ShowCalibrationDelegate = new ShowCalibrationReadings(UpdateCalibrationReadings);
            gyroTrack = new GyroTrackProc(this);
            
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            if (gyroTrack.isPortOpen())
            {
                gyroTrack.stop();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void GyroTrack_Load(object sender, EventArgs e)
        {
        }

        public void UpdateGyroTrackReadings(String myString)
        {
            textBoxReceivedStr.Text = myString;
            if (checkBoxVisual.Checked)
            {
                string[] values_tokens = myString.Split(':');
                if (values_tokens.Length == 3)
                {
                    string[] pitch_tokens = values_tokens[0].Split('.');
                    gyroControlMonitor1.setAngle(Convert.ToInt32(pitch_tokens[0]));
                    string[] roll_tokens = values_tokens[1].Split('.');
                    gyroControlMonitor2.setAngle(Convert.ToInt32(roll_tokens[0]));
                    string[] yaw_tokens = values_tokens[2].Split('.');
                    gyroControlMonitor3.setAngle(Convert.ToInt32(yaw_tokens[0]));
                }
            }

        }

        public void UpdateCalibrationReadings(String myString)
        {
            string[] values_tokens = myString.Trim().Split(':');
            if (values_tokens.Length == 6)
            {
                labelGyroXoffValue.Text = values_tokens[0];
                labelGyroYoffValue.Text = values_tokens[1];
                labelGyroZoffValue.Text = values_tokens[2];
                labelMagXoffValue.Text = values_tokens[3];
                labelMagYoffValue.Text = values_tokens[4];
                labelMagZoffValue.Text = values_tokens[5];
            }
            //else MessageBox.Show("UpdateGyroTrackReadings::error" + myString);
        }

        private void Connect_Click(object sender, EventArgs e)
        {
            if (Connect.Text == "Connect")
            {
                if (comboBoxCOMports.SelectedIndex > -1)
                {
                    if (gyroTrack.isPortOpen())
                    {
                        gyroTrack.stop();
                    }
                    string selectedCOM = comboBoxCOMports.SelectedItem.ToString();
                    gyroTrack.setSerialPort(selectedCOM);
                    try
                    {
                        if (!gyroTrack.setOpentrackIP(textBoxIPaddress.Text))
                        {
                            return;
                        }
                        if (!gyroTrack.setOpentrackPort(Convert.ToInt32(textBoxPortUDP.Text)))
                        {
                            return;
                        }
                        textBoxIPaddress.ReadOnly = true;
                        textBoxPortUDP.ReadOnly = true;
                        gyroTrack.start();
                        Connect.Text = "Disconnect";
                    }
                    catch (UnauthorizedAccessException ex)
                    {
                        MessageBox.Show("Connection error:" + ex.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Please select proper COM port first!");
                }
            }
            else
            {
                gyroTrack.stop();
                textBoxIPaddress.ReadOnly = false;
                textBoxPortUDP.ReadOnly = false;
                Connect.Text = "Connect";
            }
        }

        private void buttonCalibration_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Gyroscope Calibration, do not move sensor!");
            gyroTrack.GyroCalibration();
        }

        private void buttonCallibrationMag_Click(object sender, EventArgs e)
        {
            if (buttonCallibrationMag.Text == "Calibration Magnetometer")
            {
                MessageBox.Show("Magnetometer Calibration, rotate sensor in all XYZ angles till none of offset value is changing");
                gyroTrack.MagnetoCalibration();
                buttonCalibration.Enabled = false;
                buttonCallibrationMag.Text = "Stop Calibration";
            }
            else
            {
                gyroTrack.stopCalibration();
                buttonCalibration.Enabled = true;
                buttonCallibrationMag.Text = "Calibration Magnetometer";
            }
        }

        private void gyroControlMonitor1_Load(object sender, EventArgs e)
        {

        }
    }
}
