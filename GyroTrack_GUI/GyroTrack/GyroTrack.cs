﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Windows.Forms;


namespace GyroTrack
{
    enum GyroTrackMode { stop = 0, running = 1, calibratingGyro = 2, calibratingMag = 3, finish_calibration = 4 }

    class GyroTrackProc
    {
        SerialPort ArduinoSerial = new SerialPort();
        string arduino_send_str;

        GyroTrackForm GyroTrackFormHandle;
        GyroTrackMode GTstate;

        openTrackUDP OpenTrack;
        string OpenTrackIP;
        int OpenTrackPort;

        string arduino_data_str = "";
        string incomingData;
        int wrongDataCount = 0;

        GTdata data_to_send;


        public GyroTrackProc(GyroTrackForm myForm)
        {
            GyroTrackFormHandle = myForm;
            GTstate = GyroTrackMode.stop;
            OpenTrackIP = "127.0.0.1";
            OpenTrackPort = 4242;

            ArduinoSerial.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);
            arduino_send_str = "";
            ArduinoSerial.BaudRate = 115200;
            ArduinoSerial.Parity = Parity.None;
            ArduinoSerial.StopBits = StopBits.One;
            ArduinoSerial.DataBits = 8;
            ArduinoSerial.Handshake = Handshake.None;
            ArduinoSerial.DtrEnable = true;
            ArduinoSerial.ReadTimeout = 5000;
            ArduinoSerial.WriteTimeout = 5000;
            data_to_send.x = 0;
            data_to_send.y = 0;
            data_to_send.z = 0;
            data_to_send.yaw = 0;
            data_to_send.pitch = 0;
            data_to_send.roll = 0;
        }
        public void setSerialPort(string selectedCOM)
        {
            this.ArduinoSerial.PortName = selectedCOM;
        }
        public bool setOpentrackIP(string dest_address)
        {
            System.Net.IPAddress address;
            if (System.Net.IPAddress.TryParse(dest_address, out address))
            {
                if (address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    OpenTrackIP = dest_address;
                    return true;
                }
            }
            MessageBox.Show("Wrong ip address, try 127.0.0.1 for localhost.");
            return false;
        }
        public bool setOpentrackPort(int dest_port)
        {
            if (dest_port < 1023 || dest_port > 65534)
                MessageBox.Show("Wrong UDP port, try between 1023 and 65535.");
            else
            {
                OpenTrackPort = dest_port;
                return true;
            }
            return false;
        }
        public bool isPortOpen()
        {
            return ArduinoSerial.IsOpen;
        }
        public bool isActive()
        {
            if (GTstate == GyroTrackMode.running) return true;
            else return false;
        }
        public void start()
        {
            try
            {
                if (!ArduinoSerial.IsOpen)
                    ArduinoSerial.Open();
            }
            catch (System.IO.IOException)
            {
                MessageBox.Show("Can't connect to serial port.");
                GTstate = GyroTrackMode.stop;
                return;
            }
            OpenTrack = new openTrackUDP(OpenTrackIP, OpenTrackPort);
            GTstate = GyroTrackMode.running;
            ArduinoSerial.Write("F");
        }
        public void stop()
        {
            GTstate = GyroTrackMode.stop;
            Thread CloseDown = new Thread(new ThreadStart(CloseSerial));
            CloseDown.Start();
        }
        private void CloseSerial()
        {
            try { ArduinoSerial.Close(); }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message); 
            }
        }
        public void GyroCalibration()
        {
            GTstate = GyroTrackMode.calibratingGyro;
            arduino_send_str = "G";
        }
        public void MagnetoCalibration()
        {
            GTstate = GyroTrackMode.calibratingMag;
            arduino_send_str = "M";
        }
        public void stopCalibration()
        {
            GTstate = GyroTrackMode.finish_calibration;
            arduino_send_str = "F";
        }
        private void DataReceivedHandler(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                if (GTstate == GyroTrackMode.stop) Console.WriteLine("GyroTrackMode.stop"); ;
                incomingData += ArduinoSerial.ReadExisting();
                int endIndex = incomingData.IndexOf(Environment.NewLine);
                //Console.WriteLine("vStart Incoming: {0}, index{1}, data: \n", incomingData.Length, endIndex, incomingData);

                while (endIndex >= 0)
                {
                    if (endIndex > 0)
                    {
                        arduino_data_str = incomingData.Substring(0, endIndex);
                        incomingData = incomingData.Substring(endIndex + 1);
                        //Console.Write("arduino str: {0}\n", arduino_data_str);
                        //Console.Write("yStop Incoming str: {0}STOP\n", incomingData);
                    }
                    else if (endIndex == 0)
                    {
                        incomingData = incomingData.Substring(endIndex + 1);
                    }
                    endIndex = incomingData.IndexOf(Environment.NewLine);
                }
                if (String.IsNullOrEmpty(arduino_data_str))
                    return;

                switch (GTstate)
                {
                    case GyroTrackMode.running:
                        GyroTrackFormHandle.Invoke(GyroTrackFormHandle.ShowRawReadingsDelegate, new Object[] { arduino_data_str });
                        if (!data_to_send.update(arduino_data_str))
                        {
                            if (wrongDataCount < 6) wrongDataCount++;
                            if (wrongDataCount == 5) MessageBox.Show("Wrong data value, probably need calibration.");
                            arduino_data_str = "";
                            return;
                        }
                        wrongDataCount = 0;
                        OpenTrack.sendTrack(data_to_send);
                        break;
                    case GyroTrackMode.calibratingGyro:
                        GyroTrackFormHandle.Invoke(GyroTrackFormHandle.ShowCalibrationDelegate, new Object[] { arduino_data_str });
                        if (arduino_send_str.Length > 0)
                        {
                            ArduinoSerial.Write(arduino_send_str);
                            arduino_send_str = "";
                            arduino_data_str = "";
                        }
                        string[] values_tokens = arduino_data_str.Split(':');
                        if (values_tokens.Length == 6) { stopCalibration(); }
                        break;
                    case GyroTrackMode.calibratingMag:
                        GyroTrackFormHandle.Invoke(GyroTrackFormHandle.ShowCalibrationDelegate, new Object[] { arduino_data_str });
                        if (arduino_send_str.Length > 0)
                        {
                            ArduinoSerial.Write(arduino_send_str);
                            arduino_send_str = "";
                            arduino_data_str = "";
                        }
                        break;
                    case GyroTrackMode.finish_calibration:
                        if (arduino_send_str.Length > 0)
                        {
                            ArduinoSerial.Write(arduino_send_str);
                            GTstate = GyroTrackMode.running;
                            MessageBox.Show("Calibration stopped.");
                            arduino_send_str = "";
                        }
                        break;
                    case GyroTrackMode.stop:
                        break;
                }
                arduino_data_str = "";
            }
            catch
            {
                   if (GTstate != GyroTrackMode.stop)
                    MessageBox.Show("Read timeout. Probably wrong COM port, or device not connected.");
            }
        }
    }
}
