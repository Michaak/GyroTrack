﻿namespace GyroTrack
{
    partial class GyroTrackForm
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Wymagana metoda obsługi projektanta — nie należy modyfikować 
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GyroTrackForm));
            this.ArduinoCOM = new System.IO.Ports.SerialPort(this.components);
            this.comboBoxCOMports = new System.Windows.Forms.ComboBox();
            this.Connect = new System.Windows.Forms.Button();
            this.label_received = new System.Windows.Forms.Label();
            this.textBoxReceivedStr = new System.Windows.Forms.TextBox();
            this.labelOpenTrack = new System.Windows.Forms.Label();
            this.labelipUDP = new System.Windows.Forms.Label();
            this.labelPortUDP = new System.Windows.Forms.Label();
            this.textBoxIPaddress = new System.Windows.Forms.TextBox();
            this.textBoxPortUDP = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabRun = new System.Windows.Forms.TabPage();
            this.checkBoxVisual = new System.Windows.Forms.CheckBox();
            this.gyroControlMonitor1 = new GyroControls.GyroControlMonitor();
            this.gyroControlMonitor3 = new GyroControls.GyroControlMonitor();
            this.gyroControlMonitor2 = new GyroControls.GyroControlMonitor();
            this.tabCallibration = new System.Windows.Forms.TabPage();
            this.buttonCallibrationMag = new System.Windows.Forms.Button();
            this.buttonCalibration = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelGyroZoffValue = new System.Windows.Forms.Label();
            this.labelGyroYoffValue = new System.Windows.Forms.Label();
            this.labelGyroXoffValue = new System.Windows.Forms.Label();
            this.labelGyroscope = new System.Windows.Forms.Label();
            this.labelGyroYoffset = new System.Windows.Forms.Label();
            this.labelGyroZoffset = new System.Windows.Forms.Label();
            this.labelGyroXoffset = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelMagZoffValue = new System.Windows.Forms.Label();
            this.labelMagYoffValue = new System.Windows.Forms.Label();
            this.labelMagXoffValue = new System.Windows.Forms.Label();
            this.labelMagnetometer = new System.Windows.Forms.Label();
            this.labelMagZoffset = new System.Windows.Forms.Label();
            this.labelMagXoffset = new System.Windows.Forms.Label();
            this.labelMagYoffset = new System.Windows.Forms.Label();
            this.labelSelectCOM = new System.Windows.Forms.Label();
            this.labelPitch = new System.Windows.Forms.Label();
            this.labelRoll = new System.Windows.Forms.Label();
            this.labelYaw = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabRun.SuspendLayout();
            this.tabCallibration.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBoxCOMports
            // 
            this.comboBoxCOMports.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCOMports.FormattingEnabled = true;
            this.comboBoxCOMports.Location = new System.Drawing.Point(109, 7);
            this.comboBoxCOMports.Name = "comboBoxCOMports";
            this.comboBoxCOMports.Size = new System.Drawing.Size(63, 21);
            this.comboBoxCOMports.TabIndex = 1;
            this.comboBoxCOMports.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // Connect
            // 
            this.Connect.Location = new System.Drawing.Point(16, 16);
            this.Connect.Name = "Connect";
            this.Connect.Size = new System.Drawing.Size(75, 23);
            this.Connect.TabIndex = 2;
            this.Connect.Text = "Connect";
            this.Connect.UseVisualStyleBackColor = true;
            this.Connect.Click += new System.EventHandler(this.Connect_Click);
            // 
            // label_received
            // 
            this.label_received.AutoSize = true;
            this.label_received.Location = new System.Drawing.Point(15, 61);
            this.label_received.Name = "label_received";
            this.label_received.Size = new System.Drawing.Size(56, 13);
            this.label_received.TabIndex = 4;
            this.label_received.Text = "Received:";
            // 
            // textBoxReceivedStr
            // 
            this.textBoxReceivedStr.Location = new System.Drawing.Point(77, 58);
            this.textBoxReceivedStr.Name = "textBoxReceivedStr";
            this.textBoxReceivedStr.Size = new System.Drawing.Size(201, 20);
            this.textBoxReceivedStr.TabIndex = 5;
            this.textBoxReceivedStr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelOpenTrack
            // 
            this.labelOpenTrack.AutoSize = true;
            this.labelOpenTrack.Location = new System.Drawing.Point(13, 95);
            this.labelOpenTrack.Name = "labelOpenTrack";
            this.labelOpenTrack.Size = new System.Drawing.Size(167, 13);
            this.labelOpenTrack.TabIndex = 6;
            this.labelOpenTrack.Text = "Output UDP sender configuration:";
            // 
            // labelipUDP
            // 
            this.labelipUDP.AutoSize = true;
            this.labelipUDP.Location = new System.Drawing.Point(13, 118);
            this.labelipUDP.Name = "labelipUDP";
            this.labelipUDP.Size = new System.Drawing.Size(60, 13);
            this.labelipUDP.TabIndex = 7;
            this.labelipUDP.Text = "IP address:";
            // 
            // labelPortUDP
            // 
            this.labelPortUDP.AutoSize = true;
            this.labelPortUDP.Location = new System.Drawing.Point(166, 118);
            this.labelPortUDP.Name = "labelPortUDP";
            this.labelPortUDP.Size = new System.Drawing.Size(67, 13);
            this.labelPortUDP.TabIndex = 8;
            this.labelPortUDP.Text = "Port number:";
            // 
            // textBoxIPaddress
            // 
            this.textBoxIPaddress.Location = new System.Drawing.Point(70, 115);
            this.textBoxIPaddress.Name = "textBoxIPaddress";
            this.textBoxIPaddress.Size = new System.Drawing.Size(90, 20);
            this.textBoxIPaddress.TabIndex = 9;
            this.textBoxIPaddress.Text = "127.0.0.1";
            this.textBoxIPaddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxPortUDP
            // 
            this.textBoxPortUDP.Location = new System.Drawing.Point(239, 115);
            this.textBoxPortUDP.Name = "textBoxPortUDP";
            this.textBoxPortUDP.Size = new System.Drawing.Size(39, 20);
            this.textBoxPortUDP.TabIndex = 10;
            this.textBoxPortUDP.Text = "4242";
            this.textBoxPortUDP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabRun);
            this.tabControl1.Controls.Add(this.tabCallibration);
            this.tabControl1.Location = new System.Drawing.Point(12, 34);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(327, 291);
            this.tabControl1.TabIndex = 11;
            // 
            // tabRun
            // 
            this.tabRun.Controls.Add(this.labelYaw);
            this.tabRun.Controls.Add(this.labelRoll);
            this.tabRun.Controls.Add(this.labelPitch);
            this.tabRun.Controls.Add(this.checkBoxVisual);
            this.tabRun.Controls.Add(this.gyroControlMonitor1);
            this.tabRun.Controls.Add(this.gyroControlMonitor3);
            this.tabRun.Controls.Add(this.Connect);
            this.tabRun.Controls.Add(this.gyroControlMonitor2);
            this.tabRun.Controls.Add(this.textBoxPortUDP);
            this.tabRun.Controls.Add(this.textBoxIPaddress);
            this.tabRun.Controls.Add(this.label_received);
            this.tabRun.Controls.Add(this.labelPortUDP);
            this.tabRun.Controls.Add(this.textBoxReceivedStr);
            this.tabRun.Controls.Add(this.labelipUDP);
            this.tabRun.Controls.Add(this.labelOpenTrack);
            this.tabRun.Location = new System.Drawing.Point(4, 22);
            this.tabRun.Name = "tabRun";
            this.tabRun.Padding = new System.Windows.Forms.Padding(3);
            this.tabRun.Size = new System.Drawing.Size(319, 265);
            this.tabRun.TabIndex = 0;
            this.tabRun.Text = "Run";
            this.tabRun.UseVisualStyleBackColor = true;
            // 
            // checkBoxVisual
            // 
            this.checkBoxVisual.AutoSize = true;
            this.checkBoxVisual.Location = new System.Drawing.Point(16, 145);
            this.checkBoxVisual.Name = "checkBoxVisual";
            this.checkBoxVisual.Size = new System.Drawing.Size(119, 17);
            this.checkBoxVisual.TabIndex = 16;
            this.checkBoxVisual.Text = "Enable visualization";
            this.checkBoxVisual.UseVisualStyleBackColor = true;
            // 
            // gyroControlMonitor1
            // 
            this.gyroControlMonitor1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.gyroControlMonitor1.Location = new System.Drawing.Point(18, 168);
            this.gyroControlMonitor1.Name = "gyroControlMonitor1";
            this.gyroControlMonitor1.Size = new System.Drawing.Size(80, 80);
            this.gyroControlMonitor1.TabIndex = 13;
            // 
            // gyroControlMonitor3
            // 
            this.gyroControlMonitor3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.gyroControlMonitor3.Location = new System.Drawing.Point(220, 168);
            this.gyroControlMonitor3.Name = "gyroControlMonitor3";
            this.gyroControlMonitor3.Size = new System.Drawing.Size(80, 80);
            this.gyroControlMonitor3.TabIndex = 15;
            // 
            // gyroControlMonitor2
            // 
            this.gyroControlMonitor2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.gyroControlMonitor2.Location = new System.Drawing.Point(119, 168);
            this.gyroControlMonitor2.Name = "gyroControlMonitor2";
            this.gyroControlMonitor2.Size = new System.Drawing.Size(80, 80);
            this.gyroControlMonitor2.TabIndex = 14;
            // 
            // tabCallibration
            // 
            this.tabCallibration.Controls.Add(this.buttonCallibrationMag);
            this.tabCallibration.Controls.Add(this.buttonCalibration);
            this.tabCallibration.Controls.Add(this.panel1);
            this.tabCallibration.Controls.Add(this.panel2);
            this.tabCallibration.Location = new System.Drawing.Point(4, 22);
            this.tabCallibration.Name = "tabCallibration";
            this.tabCallibration.Padding = new System.Windows.Forms.Padding(3);
            this.tabCallibration.Size = new System.Drawing.Size(319, 265);
            this.tabCallibration.TabIndex = 1;
            this.tabCallibration.Text = "Callibration";
            this.tabCallibration.UseVisualStyleBackColor = true;
            // 
            // buttonCallibrationMag
            // 
            this.buttonCallibrationMag.Location = new System.Drawing.Point(162, 16);
            this.buttonCallibrationMag.Name = "buttonCallibrationMag";
            this.buttonCallibrationMag.Size = new System.Drawing.Size(150, 23);
            this.buttonCallibrationMag.TabIndex = 15;
            this.buttonCallibrationMag.Text = "Calibration Magnetometer";
            this.buttonCallibrationMag.UseVisualStyleBackColor = true;
            this.buttonCallibrationMag.Click += new System.EventHandler(this.buttonCallibrationMag_Click);
            // 
            // buttonCalibration
            // 
            this.buttonCalibration.Location = new System.Drawing.Point(6, 16);
            this.buttonCalibration.Name = "buttonCalibration";
            this.buttonCalibration.Size = new System.Drawing.Size(140, 23);
            this.buttonCalibration.TabIndex = 0;
            this.buttonCalibration.Text = "Calibration Gyroscope";
            this.buttonCalibration.UseVisualStyleBackColor = true;
            this.buttonCalibration.Click += new System.EventHandler(this.buttonCalibration_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.Controls.Add(this.labelGyroZoffValue);
            this.panel1.Controls.Add(this.labelGyroYoffValue);
            this.panel1.Controls.Add(this.labelGyroXoffValue);
            this.panel1.Controls.Add(this.labelGyroscope);
            this.panel1.Controls.Add(this.labelGyroYoffset);
            this.panel1.Controls.Add(this.labelGyroZoffset);
            this.panel1.Controls.Add(this.labelGyroXoffset);
            this.panel1.Location = new System.Drawing.Point(6, 45);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(306, 53);
            this.panel1.TabIndex = 13;
            // 
            // labelGyroZoffValue
            // 
            this.labelGyroZoffValue.AutoSize = true;
            this.labelGyroZoffValue.Location = new System.Drawing.Point(256, 27);
            this.labelGyroZoffValue.Name = "labelGyroZoffValue";
            this.labelGyroZoffValue.Size = new System.Drawing.Size(25, 13);
            this.labelGyroZoffValue.TabIndex = 7;
            this.labelGyroZoffValue.Text = "------";
            this.labelGyroZoffValue.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelGyroYoffValue
            // 
            this.labelGyroYoffValue.AutoSize = true;
            this.labelGyroYoffValue.Location = new System.Drawing.Point(155, 27);
            this.labelGyroYoffValue.Name = "labelGyroYoffValue";
            this.labelGyroYoffValue.Size = new System.Drawing.Size(25, 13);
            this.labelGyroYoffValue.TabIndex = 6;
            this.labelGyroYoffValue.Text = "------";
            this.labelGyroYoffValue.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelGyroXoffValue
            // 
            this.labelGyroXoffValue.AutoSize = true;
            this.labelGyroXoffValue.Location = new System.Drawing.Point(59, 27);
            this.labelGyroXoffValue.Name = "labelGyroXoffValue";
            this.labelGyroXoffValue.Size = new System.Drawing.Size(25, 13);
            this.labelGyroXoffValue.TabIndex = 5;
            this.labelGyroXoffValue.Text = "------";
            this.labelGyroXoffValue.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelGyroscope
            // 
            this.labelGyroscope.AutoSize = true;
            this.labelGyroscope.Location = new System.Drawing.Point(3, 0);
            this.labelGyroscope.Name = "labelGyroscope";
            this.labelGyroscope.Size = new System.Drawing.Size(61, 13);
            this.labelGyroscope.TabIndex = 4;
            this.labelGyroscope.Text = "Gyroscope:";
            // 
            // labelGyroYoffset
            // 
            this.labelGyroYoffset.AutoSize = true;
            this.labelGyroYoffset.Location = new System.Drawing.Point(103, 27);
            this.labelGyroYoffset.Name = "labelGyroYoffset";
            this.labelGyroYoffset.Size = new System.Drawing.Size(46, 13);
            this.labelGyroYoffset.TabIndex = 2;
            this.labelGyroYoffset.Text = "Y offset:";
            // 
            // labelGyroZoffset
            // 
            this.labelGyroZoffset.AutoSize = true;
            this.labelGyroZoffset.Location = new System.Drawing.Point(204, 27);
            this.labelGyroZoffset.Name = "labelGyroZoffset";
            this.labelGyroZoffset.Size = new System.Drawing.Size(46, 13);
            this.labelGyroZoffset.TabIndex = 3;
            this.labelGyroZoffset.Text = "Z offset:";
            // 
            // labelGyroXoffset
            // 
            this.labelGyroXoffset.AutoSize = true;
            this.labelGyroXoffset.Location = new System.Drawing.Point(7, 27);
            this.labelGyroXoffset.Name = "labelGyroXoffset";
            this.labelGyroXoffset.Size = new System.Drawing.Size(46, 13);
            this.labelGyroXoffset.TabIndex = 1;
            this.labelGyroXoffset.Text = "X offset:";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.labelMagZoffValue);
            this.panel2.Controls.Add(this.labelMagYoffValue);
            this.panel2.Controls.Add(this.labelMagXoffValue);
            this.panel2.Controls.Add(this.labelMagnetometer);
            this.panel2.Controls.Add(this.labelMagZoffset);
            this.panel2.Controls.Add(this.labelMagXoffset);
            this.panel2.Controls.Add(this.labelMagYoffset);
            this.panel2.Location = new System.Drawing.Point(6, 101);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(306, 53);
            this.panel2.TabIndex = 14;
            // 
            // labelMagZoffValue
            // 
            this.labelMagZoffValue.AutoSize = true;
            this.labelMagZoffValue.Location = new System.Drawing.Point(256, 31);
            this.labelMagZoffValue.Name = "labelMagZoffValue";
            this.labelMagZoffValue.Size = new System.Drawing.Size(25, 13);
            this.labelMagZoffValue.TabIndex = 10;
            this.labelMagZoffValue.Text = "------";
            this.labelMagZoffValue.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelMagYoffValue
            // 
            this.labelMagYoffValue.AutoSize = true;
            this.labelMagYoffValue.Location = new System.Drawing.Point(155, 31);
            this.labelMagYoffValue.Name = "labelMagYoffValue";
            this.labelMagYoffValue.Size = new System.Drawing.Size(25, 13);
            this.labelMagYoffValue.TabIndex = 9;
            this.labelMagYoffValue.Text = "------";
            this.labelMagYoffValue.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelMagXoffValue
            // 
            this.labelMagXoffValue.AutoSize = true;
            this.labelMagXoffValue.Location = new System.Drawing.Point(59, 31);
            this.labelMagXoffValue.Name = "labelMagXoffValue";
            this.labelMagXoffValue.Size = new System.Drawing.Size(25, 13);
            this.labelMagXoffValue.TabIndex = 8;
            this.labelMagXoffValue.Text = "------";
            this.labelMagXoffValue.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelMagnetometer
            // 
            this.labelMagnetometer.AutoSize = true;
            this.labelMagnetometer.Location = new System.Drawing.Point(3, 0);
            this.labelMagnetometer.Name = "labelMagnetometer";
            this.labelMagnetometer.Size = new System.Drawing.Size(78, 13);
            this.labelMagnetometer.TabIndex = 7;
            this.labelMagnetometer.Text = "Magnetometer:";
            // 
            // labelMagZoffset
            // 
            this.labelMagZoffset.AutoSize = true;
            this.labelMagZoffset.Location = new System.Drawing.Point(204, 31);
            this.labelMagZoffset.Name = "labelMagZoffset";
            this.labelMagZoffset.Size = new System.Drawing.Size(46, 13);
            this.labelMagZoffset.TabIndex = 6;
            this.labelMagZoffset.Text = "Z offset:";
            // 
            // labelMagXoffset
            // 
            this.labelMagXoffset.AutoSize = true;
            this.labelMagXoffset.Location = new System.Drawing.Point(7, 31);
            this.labelMagXoffset.Name = "labelMagXoffset";
            this.labelMagXoffset.Size = new System.Drawing.Size(46, 13);
            this.labelMagXoffset.TabIndex = 4;
            this.labelMagXoffset.Text = "X offset:";
            // 
            // labelMagYoffset
            // 
            this.labelMagYoffset.AutoSize = true;
            this.labelMagYoffset.Location = new System.Drawing.Point(103, 31);
            this.labelMagYoffset.Name = "labelMagYoffset";
            this.labelMagYoffset.Size = new System.Drawing.Size(46, 13);
            this.labelMagYoffset.TabIndex = 5;
            this.labelMagYoffset.Text = "Y offset:";
            // 
            // labelSelectCOM
            // 
            this.labelSelectCOM.AutoSize = true;
            this.labelSelectCOM.Location = new System.Drawing.Point(13, 10);
            this.labelSelectCOM.Name = "labelSelectCOM";
            this.labelSelectCOM.Size = new System.Drawing.Size(90, 13);
            this.labelSelectCOM.TabIndex = 12;
            this.labelSelectCOM.Text = "Input device port:";
            // 
            // labelPitch
            // 
            this.labelPitch.AutoSize = true;
            this.labelPitch.BackColor = System.Drawing.Color.Snow;
            this.labelPitch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelPitch.Location = new System.Drawing.Point(40, 210);
            this.labelPitch.Name = "labelPitch";
            this.labelPitch.Size = new System.Drawing.Size(33, 15);
            this.labelPitch.TabIndex = 17;
            this.labelPitch.Text = "Pitch";
            // 
            // labelRoll
            // 
            this.labelRoll.AutoSize = true;
            this.labelRoll.BackColor = System.Drawing.Color.Snow;
            this.labelRoll.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelRoll.Location = new System.Drawing.Point(144, 210);
            this.labelRoll.Name = "labelRoll";
            this.labelRoll.Size = new System.Drawing.Size(27, 15);
            this.labelRoll.TabIndex = 18;
            this.labelRoll.Text = "Roll";
            // 
            // labelYaw
            // 
            this.labelYaw.AutoSize = true;
            this.labelYaw.BackColor = System.Drawing.Color.Snow;
            this.labelYaw.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelYaw.Location = new System.Drawing.Point(245, 210);
            this.labelYaw.Name = "labelYaw";
            this.labelYaw.Size = new System.Drawing.Size(30, 15);
            this.labelYaw.TabIndex = 19;
            this.labelYaw.Text = "Yaw";
            // 
            // GyroTrackForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(348, 332);
            this.Controls.Add(this.labelSelectCOM);
            this.Controls.Add(this.comboBoxCOMports);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GyroTrackForm";
            this.Text = "GyroTrack";
            this.Load += new System.EventHandler(this.GyroTrack_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabRun.ResumeLayout(false);
            this.tabRun.PerformLayout();
            this.tabCallibration.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.IO.Ports.SerialPort ArduinoCOM;
        private System.Windows.Forms.ComboBox comboBoxCOMports;
        private System.Windows.Forms.Button Connect;
        private System.Windows.Forms.Label label_received;
        private System.Windows.Forms.TextBox textBoxReceivedStr;
        private System.Windows.Forms.Label labelOpenTrack;
        private System.Windows.Forms.Label labelipUDP;
        private System.Windows.Forms.Label labelPortUDP;
        private System.Windows.Forms.TextBox textBoxIPaddress;
        private System.Windows.Forms.TextBox textBoxPortUDP;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabRun;
        private System.Windows.Forms.TabPage tabCallibration;
        private System.Windows.Forms.Label labelSelectCOM;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelMagZoffset;
        private System.Windows.Forms.Label labelMagYoffset;
        private System.Windows.Forms.Label labelMagXoffset;
        private System.Windows.Forms.Label labelGyroZoffset;
        private System.Windows.Forms.Label labelGyroYoffset;
        private System.Windows.Forms.Label labelGyroXoffset;
        private System.Windows.Forms.Button buttonCalibration;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label labelGyroscope;
        private System.Windows.Forms.Label labelMagnetometer;
        private System.Windows.Forms.Label labelGyroZoffValue;
        private System.Windows.Forms.Label labelGyroYoffValue;
        private System.Windows.Forms.Label labelGyroXoffValue;
        private System.Windows.Forms.Label labelMagZoffValue;
        private System.Windows.Forms.Label labelMagYoffValue;
        private System.Windows.Forms.Label labelMagXoffValue;
        private System.Windows.Forms.Button buttonCallibrationMag;
        private GyroControls.GyroControlMonitor gyroControlMonitor1;
        private GyroControls.GyroControlMonitor gyroControlMonitor3;
        private GyroControls.GyroControlMonitor gyroControlMonitor2;
        private System.Windows.Forms.CheckBox checkBoxVisual;
        private System.Windows.Forms.Label labelYaw;
        private System.Windows.Forms.Label labelRoll;
        private System.Windows.Forms.Label labelPitch;
    }
}

